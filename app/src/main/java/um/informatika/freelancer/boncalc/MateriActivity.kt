package um.informatika.freelancer.boncalc

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class MateriActivity : AppCompatActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, MateriActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_materi)
        supportActionBar?.setSubtitle(R.string.materi_text_subtitle)
    }
}

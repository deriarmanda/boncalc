package um.informatika.freelancer.boncalc

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_calculator.*

class CalculatorActivity : AppCompatActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, CalculatorActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculator)

        prepareForm()
        button_hitung.setOnClickListener { calculate() }
    }

    private fun prepareForm() {
        form_tempat_tidur.setOnFocusChangeListener { view, hasFocus ->
            view as EditText
            if (!hasFocus && view.text.isNullOrBlank()) view.setText(R.string.calc_text_default_0)
        }
        form_periode.setOnFocusChangeListener { view, hasFocus ->
            view as EditText
            if (!hasFocus && view.text.isNullOrBlank()) view.setText(R.string.calc_text_default_0)
        }
        form_perawatan.setOnFocusChangeListener { view, hasFocus ->
            view as EditText
            if (!hasFocus && view.text.isNullOrBlank()) view.setText(R.string.calc_text_default_0)
        }
        form_dirawat.setOnFocusChangeListener { view, hasFocus ->
            view as EditText
            if (!hasFocus && view.text.isNullOrBlank()) view.setText(R.string.calc_text_default_0)
        }
        form_pasien_hidup.setOnFocusChangeListener { view, hasFocus ->
            view as EditText
            if (!hasFocus && view.text.isNullOrBlank()) view.setText(R.string.calc_text_default_0)
        }
        form_pasien_mati_kurang.setOnFocusChangeListener { view, hasFocus ->
            view as EditText
            if (!hasFocus && view.text.isNullOrBlank()) view.setText(R.string.calc_text_default_0)
        }
        form_pasien_mati_lebih.setOnFocusChangeListener { view, hasFocus ->
            view as EditText
            if (!hasFocus && view.text.isNullOrBlank()) view.setText(R.string.calc_text_default_0)
        }
    }

    private fun calculate() {
        val tempatTidur = Integer.parseInt(form_tempat_tidur.text.toString()).toFloat()
        val periode = Integer.parseInt(form_periode.text.toString()).toFloat()
        val perawatan = Integer.parseInt(form_perawatan.text.toString()).toFloat()
        val dirawat = Integer.parseInt(form_dirawat.text.toString()).toFloat()
        val pasienHidup = Integer.parseInt(form_pasien_hidup.text.toString()).toFloat()
        val pasienMatiKurang = Integer.parseInt(form_pasien_mati_kurang.text.toString()).toFloat()
        val pasienMatiLebih = Integer.parseInt(form_pasien_mati_lebih.text.toString()).toFloat()
        val pasienMati: Float = pasienMatiKurang + pasienMatiLebih

        // BOR
        val hasilBor = (perawatan) / (tempatTidur * periode)
        text_hasil_bor.text = String.format(
            "%.2f",
            hasilBor
        )

        // ALOS
        val hasilAlos = (dirawat) / (pasienHidup + pasienMati)
        text_hasil_alos.text = String.format(
            "%.2f",
            hasilAlos
        )

        // BTO
        val hasilBto = (pasienHidup + pasienMati) / (tempatTidur)
        text_hasil_bto.text = String.format(
            "%.2f",
            hasilBto
        )

        // TOI
        val hasilToi = ((tempatTidur * periode) - perawatan) / (pasienHidup + pasienMati)
        text_hasil_toi.text = String.format(
            "%.2f",
            hasilToi
        )

        // NDR
        val hasilNdr = (pasienMatiLebih) / (pasienHidup + pasienMati)
        text_hasil_ndr.text = String.format(
            "%.2f",
            hasilNdr
        )

        // GDR
        val hasilGdr = (pasienMati) / (pasienHidup + pasienMati)
        text_hasil_gdr.text = String.format(
            "%.2f",
            hasilGdr
        )

//        text_title_bor.parent.requestChildFocus(text_title_bor, text_title_bor)
        scroll_view.post {
            scroll_view.smoothScrollTo(0, button_hitung.bottom + 16)
        }
    }
}

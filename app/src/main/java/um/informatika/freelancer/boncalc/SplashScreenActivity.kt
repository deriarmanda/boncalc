package um.informatika.freelancer.boncalc

import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val timer = object : CountDownTimer(1000, 1000) {
            override fun onTick(p0: Long) {}
            override fun onFinish() {
                val context = this@SplashScreenActivity
                startActivity(HomeActivity.getIntent(context))
                finish()
            }
        }
        timer.start()
    }
}

package um.informatika.freelancer.boncalc

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, HomeActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        menu_materi.setOnClickListener { startActivity(MateriActivity.getIntent(this)) }
        menu_calculator.setOnClickListener { startActivity(CalculatorActivity.getIntent(this)) }
        image_app.setOnClickListener { startActivity(AboutActivity.getIntent(this)) }
    }
}
